package ptit.edu.vn.SQA.Controller.v1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ptit.edu.vn.SQA.controller.api.v1.ConfigController;
import ptit.edu.vn.SQA.controller.api.v1.ConfigPriceController;
import ptit.edu.vn.SQA.model.ConfigPrice;
import ptit.edu.vn.SQA.repository.ConfigPriceRepository;
import ptit.edu.vn.SQA.repository.ConfigRepository;
import ptit.edu.vn.SQA.service.ConfigPriceService;
import ptit.edu.vn.SQA.service.ConfigService;
import ptit.edu.vn.SQA.service.Impl.ConfigPriceServiceImpl;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ConfigPriceController.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("ConfigPriceControllerTest")
public class ConfigPriceControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    @TestConfiguration
    public static class ServiceTestConfiguration {
        @Bean
        ConfigPriceService configPriceService() {
            return new ConfigPriceServiceImpl();
        }
    }
    @Autowired
    MockMvc mockMvc;
    @MockBean
    ConfigRepository configRepository;
    @MockBean
    ConfigPriceRepository configPriceRepository;
    @MockBean
    ConfigService configService;
    // test get all lịch sử thay đổi giá của bậc cấu hình thành công, code 200
    @Test
    public void testGetListProductPriceSuccess() throws Exception {
        List<ConfigPrice> list = new ArrayList<>();
        for (int i=0;i<5;i++){
            ConfigPrice configPrice = new ConfigPrice().setConfigLevel("A").setIsActive(true).setPrice(BigDecimal.valueOf(10f))
                    .setCreateAt(Instant.now()).setId("a"+i).setUpdateAt(Instant.now());
            list.add(configPrice);
        }
        Pageable pageable= PageRequest.of(0,20);
        Page<ConfigPrice> rs= new PageImpl<ConfigPrice>(list,pageable,list.size());

        given(configRepository.existsByLevel("A")).willReturn(true);
        given(configPriceRepository.getAllByCondition("A",null,null,null,pageable)).willReturn(rs);
        mockMvc.perform(get("/v1/configs/{config_level}/config-prices","A"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_200"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.total_items").value("5"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("OK"));

    }
    // test get all lịch sử của bậc cấu hình thất bại do sai bậc
    @Test
    public void testGetFalseWithNotExistsConfigLevel() throws Exception{
        given(configRepository.existsByLevel("AA")).willReturn(false);
        mockMvc.perform(get("/v1/configs/{config_level}/config-prices","AA").contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_400"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bậc nước không tồn tại"));

    }


}
