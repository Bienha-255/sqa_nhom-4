package ptit.edu.vn.SQA.Controller.v1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ptit.edu.vn.SQA.controller.api.v1.BillController;
import ptit.edu.vn.SQA.controller.api.v1.ConfigController;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Config;
import ptit.edu.vn.SQA.model.ConfigPrice;
import ptit.edu.vn.SQA.repository.ConfigPriceRepository;
import ptit.edu.vn.SQA.repository.ConfigRepository;
import ptit.edu.vn.SQA.service.BillService;
import ptit.edu.vn.SQA.service.ConfigService;
import ptit.edu.vn.SQA.service.Impl.BillServiceImpl;
import ptit.edu.vn.SQA.service.Impl.ConfigServiceImpl;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ConfigController.class)
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ConfigControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    @TestConfiguration
    public static class ServiceTestConfiguration{
        @Bean
        ConfigService configService(){return new ConfigServiceImpl();
        }
    }
    @Autowired
    MockMvc mockMvc;
    @MockBean
    ConfigRepository configRepository;
    @MockBean
    ConfigPriceRepository configPriceRepository;
    private String jsonCreateRequest= " {\n" +
            "            \"id\": \"4028819078fd17990178fe1ec3d10000\",\n" +
            "            \"explain\": \" 10m3 khối nước sạch đầu tiên\",\n" +
            "            \"price\": 5973.0,\n" +
            "            \"level\": \"1\",\n" +
            "            \"number_min\": 0.0,\n" +
            "            \"number_max\": 10.0\n" +
            "        }";
    private String jsonUpdateRequest= " {\n" +
            "            \"id\": \"4028819078fd17990178fe1ec3d10000\",\n" +
            "            \"explain\": \" 10m3 khối nước sạch đầu tiên\",\n" +
            "            \"price\": 5973.0,\n" +
            "            \"level\": \"1\",\n" +
            "            \"number_min\": 0.0,\n" +
            "            \"number_max\": 10.0\n" +
            "        }";
// test get all tất cả cấu hình thành công, code 200
    @Test
    public void testGetAllConfig() throws Exception
    {
        List<Config> configs = new ArrayList<>();
        for(int i=0;i<5;i++){
            Config config = new Config().setId("A"+i).setLevel("A"+i).setPrice(BigDecimal.valueOf(10f)).setExplain("Explain"+i)
                    .setNumberMin(0+i).setNumberMax(10+i);
            configs.add(config);
        }
        Pageable pageable= PageRequest.of(0,20);
        Page<Config> result =new PageImpl<Config>(configs,pageable,configs.size());

        given(configRepository.getAllByCondition(null,null,pageable)).willReturn( result);
        mockMvc.perform(get("/v1/configs"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_200"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.total_items").value("5"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].id").value("A0"));

    }
    // test tạo cấu hình thành công với bậc giá mới, code 201
    @Test
    public void testCreateConfigSuccess() throws Exception {
        given(configRepository.save(isA(Config.class))).willAnswer(i -> i.getArgument(0));
        given(configRepository.existsByLevel("1")).willReturn(false);
        mockMvc.perform(post("/v1/configs").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_201"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("CREATED"));

    }
    // test tạo cấu hình thất bại với bậc cấu hình đã tồn tại, code 400
    @Test
    public void testCreateConfigFalseWithConfigLevel() throws Exception{
        given(configRepository.existsByLevel("1")).willReturn(true);
        mockMvc.perform(post("/v1/configs").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_400"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bậc nước đã tồn tại"));

    }
    // test cập nhật cấu hình theo bậc thành công với bậc nước hợp lệ, code 200
    @Test
    public void testUpdateConfigSuccess()throws Exception{
        Config config = new Config().setId("A").setLevel("A").setPrice(BigDecimal.valueOf(10f)).setExplain("Explain")
                .setNumberMin(0).setNumberMax(10);
        ConfigPrice configPrice = new ConfigPrice().setConfigLevel("A").setIsActive(true).setId("1");
        given(configRepository.findByLevel("A")).willReturn(Optional.of(config));
        given(configPriceRepository.save(isA(ConfigPrice.class))).willAnswer(i->i.getArgument(0));
        given(configPriceRepository.findLatestConfigPrice("A")).willReturn(configPrice);
        given(configRepository.save(isA(Config.class))).willAnswer(i -> i.getArgument(0));
        mockMvc.perform(patch("/v1/configs/{config_level}","A").contentType(APPLICATION_JSON_UTF8).content(jsonUpdateRequest))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_200"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("OK"));
    }
    // test cập nhật bậc nước thất bại với bậc nước không tồn tại, code 404
    @Test
    public void testUpdateFalseWithConfigLevelNotExists() throws Exception{
        given(configRepository.findByLevel("A")).willReturn(Optional.empty());
        mockMvc.perform(patch("/v1/configs/{config_level}","A").contentType(APPLICATION_JSON_UTF8).content(jsonUpdateRequest))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_404"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Ma khong ton tai"));
    }
}
