package ptit.edu.vn.SQA.Controller.v1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.controller.api.v1.BillController;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Customer;
import ptit.edu.vn.SQA.repository.BillDetailRepository;
import ptit.edu.vn.SQA.repository.BillRepository;
import ptit.edu.vn.SQA.repository.CustomerRepository;
import ptit.edu.vn.SQA.service.BillService;
import ptit.edu.vn.SQA.service.Impl.BillServiceImpl;


import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BillController.class)
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class BillControllerTest
{
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    @TestConfiguration
    public static class ServiceTestConfiguration{
        @Bean
        BillService billService(){return new BillServiceImpl();
        }
    }
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    BillRepository billRepository;
    @MockBean
    BillDetailRepository billDetailRepository;
    @MockBean
    CustomerRepository customerRepository;

    // khởi tạo request
    private String jsonCreateRequest= " {\n" +
            "            \"amount\": \"50\",\n" +
            "            \"start_date\": \"1622084400\",\n" +
            "            \"finish_date\": \"1622084400\",\n" +
            "            \"new_number\": \"0\",\n" +
            "            \"old_number\": \"50\",\n" +
            "            \"total_price\": \"100000.0\",\n" +
            "            \"payment_date\": \"1622084400\",\n" +
            "            \"status\": \"CREATE\",\n" +
            "            \"create_at\": \"1622084400\" \n" +
            "        }";
    private String jsonUpdateRequest= " {\n" +
            "            \"customer_code\": \"C01\",\n" +
            "            \"amount\": \"50\",\n" +
            "            \"start_date\": \"1622084400\",\n" +
            "            \"finish_date\": \"1622084400\",\n" +
            "            \"new_number\": \"0\",\n" +
            "            \"old_number\": \"50\",\n" +
            "            \"total_price\": \"100000.0\",\n" +
            "            \"payment_date\": \"622084400\",\n" +
            "            \"status\": \"CREATE\" \n" +
            "        }";
// test get all hóa đơn của khách hàng thành công với dữ liệu tạo trong, code 200
    @Test
    public void testGetAllBillWithCustomerCodeSuccess() throws Exception{
        List<Bill> bills = new ArrayList<>();
        for(int i=0;i<5;i++){
            Bill bill = new Bill().setId("A"+i).setCustomerCode("A1").setAmount(10f).setCreateAt(Instant.now()).setFinishDate(Instant.now())
                    .setStartDate(Instant.now()).setNewNumber(10f).setOldNumber(0f).setMonth(4).setYear(2021);
            bills.add(bill);
        }
        Pageable pageable= PageRequest.of(0,20);
        Page<Bill> result =new PageImpl<Bill>(bills,pageable,bills.size());

        given(billRepository.getAllByCondition("A1",null,null,null,null,null,pageable)).willReturn( result);
        mockMvc.perform(get("/v1/customers/{customer_code}/bills","A1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_200"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.total_items").value("5"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].id").value("A0"));
    }
// test get all hóa đơn của tất cả khách hàng , code 200
    @Test
    public void testGetAllBills() throws Exception{
        List<Bill> bills = new ArrayList<>();
        List<Object[]> objects = new ArrayList<>();
        for(int i=0;i<5;i++){
            Bill bill = new Bill().setId("A"+i).setCustomerCode("A"+i).setAmount(10f).setCreateAt(Instant.now()).setFinishDate(Instant.now())
                    .setStartDate(Instant.now()).setNewNumber(10f).setOldNumber(0f).setMonth(4).setYear(2021);
            bills.add(bill);
            Customer customer = new Customer().setId("A"+i).setName("name"+i).setCode("A"+i)
                    .setAddress("address"+i).setPhone("09231"+i).setDistric("tx"+i)
                    .setEmail("email"+i).setActiveAt(Instant.now()).setTotalAmount(BigDecimal.ZERO.add(BigDecimal.valueOf(i)));

            Object[] object = new Object[10];
            object[0] = bill;
            object[1] = customer;
            objects.add(object);
        }
        Pageable pageable= PageRequest.of(0,20);
        MultiValueMap<String, String> where = new LinkedMultiValueMap<>();
        where.add("embed","customers");

        ResultPage<Object[]> resultPage=new ResultPage<>();
        resultPage.setPageList(objects);
        resultPage.setTotalItems(5);

        given(billDetailRepository.getAllWithFilter(pageable,where)).willReturn(resultPage);
        mockMvc.perform(get("/v1/bills").params(where)).andExpect(status().isOk())
                .andExpect(jsonPath("$.code").value("R_200"))
                .andExpect(jsonPath("$.total_items").value(5))
                .andExpect(jsonPath("$.items[0].id").value("A0"));
    }
    // test tạo hóa đơn cho 1 khách hàng cụ thể thành công, code 201
    @Test
    public void testCreateBillSuccess() throws Exception {
        given(billRepository.save(isA(Bill.class))).willAnswer(i -> i.getArgument(0));
        given(customerRepository.existsByCode("C01")).willReturn(true);
        mockMvc.perform(post("/v1/customers/{customer_code}/bills","C01").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_201"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("CREATED"));

    }

    // test tạo hóa đơn thất bại với mã khách hàng không tồn tại, code 404
    @Test
    public void testCreateBillFalseWithNotExistsCustomerCode() throws Exception{
        given(customerRepository.existsByCode("HGHJkj")).willReturn(false);
        mockMvc.perform(post("/v1/customers/{customer_code}/bills","HGHJkj").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_404"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Mã khách hàng không tồn tại"));
    }
//  test cập nhật thất bại với  mã hóa đơn  không tồn tại, code 404
    @Test
    public void testUpdateBillFalseWithBillIdNotExists() throws Exception {
        given(billRepository.findById("10001")).willReturn(Optional.empty());
        mockMvc.perform(patch("/v1/bills/{bill_id}","10001").contentType(APPLICATION_JSON_UTF8).content(jsonUpdateRequest))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_404"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Mã hóa đơn không tồn tại"));

    }
// tets nhập nhập thành công với mã hóa đơn hợp lệ, code 200
    @Test
    public void testUpdateBillSuccess()throws Exception{
        Bill bill = new Bill().setId("402880e779983e910179983fa08b0000").setCustomerCode("C01").setAmount(10f).setCreateAt(Instant.now()).setFinishDate(Instant.now())
                .setStartDate(Instant.now()).setNewNumber(10f).setOldNumber(0f).setMonth(4).setYear(2021).setPaymentDate(Instant.now()).setStatus("CREATED")
                .setTotalPrice(BigDecimal.valueOf(100));
        given(billRepository.findById("402880e779983e910179983fa08b0000")).willReturn(Optional.of(bill));
        given(billRepository.save(isA(Bill.class))).willAnswer(i -> i.getArgument(0));
        mockMvc.perform(patch("/v1/bills/402880e779983e910179983fa08b0000").contentType(APPLICATION_JSON_UTF8).content(jsonUpdateRequest))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_200"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("OK"));
    }
}
