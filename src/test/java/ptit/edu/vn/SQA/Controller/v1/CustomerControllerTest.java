package ptit.edu.vn.SQA.Controller.v1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.controller.api.v1.CustomerController;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Customer;
import ptit.edu.vn.SQA.repository.BillRepository;
import ptit.edu.vn.SQA.repository.CustomerDetailRepository;
import ptit.edu.vn.SQA.repository.CustomerRepository;
import ptit.edu.vn.SQA.service.CustomerService;
import ptit.edu.vn.SQA.service.Impl.CustomerServiceImpl;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CustomerControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    @TestConfiguration
    public static class ServiceTestConfiguration{
        @Bean
        CustomerService customerService(){return new CustomerServiceImpl();
        }
    }
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private CustomerDetailRepository customerDetailRepository;
    @MockBean
    private BillRepository billRepository;

    // khởi tạo giá trị request với định dạng json
    private String jsonCreateRequest= " {\n" +
            "            \"id\": \"2c93492b78f8369a0178f83c46b50005\",\n" +
            "            \"name\": \"Hà Đăng Biên\",\n" +
            "            \"distric\": \"Nam Từ Liêm\",\n" +
            "            \"phone\": \"0363023933\",\n" +
            "            \"address\": \"Ngõ 2 Đại Lộ Thăng Long\",\n" +
            "            \"email\": \"habien255@gmail.com\",\n" +
            "            \"code\": \"C01\",\n" +
            "            \"total_amount\": 100.0,\n" +
            "            \"active_at\": null,\n" +
            "            \"bills\": null\n" +
            "        }";
    private String jsonUpdateRequest= " {\n" +
            "            \"name\": \"Hà Đăng Biên\",\n" +
            "            \"distric\": \"Nam Từ Liêm\",\n" +
            "            \"phone\": \"0363023933\",\n" +
            "            \"address\": \"Ngõ 2 Đại Lộ Thăng Long\",\n" +
            "            \"email\": \"habien255@gmail.com\",\n" +
            "            \"code\": \"C01\",\n" +
            "            \"total_amount\": 100.0,\n" +
            "            \"bills\": null\n" +
            "        }";
// test get all khách hàng thành công , code 200
    @Test
    public void testGetCustomerListSuccess()throws Exception{
        List<Customer> customers = new ArrayList<>();
        for(int i=0;i<5;i++){
            Customer customer = new Customer().setId("A"+i).setName("name"+i)
                    .setAddress("address"+i).setPhone("09231"+i).setDistric("tx"+i)
                    .setEmail("email"+i).setActiveAt(Instant.now()).setTotalAmount(BigDecimal.ZERO.add(BigDecimal.valueOf(i)));
            customers.add(customer);
        }
        Pageable pageable= PageRequest.of(0,20);
        MultiValueMap<String, String> where = new LinkedMultiValueMap<>();
        ResultPage<Customer> resultPage=new ResultPage<>();
        resultPage.setPageList(customers);
        resultPage.setTotalItems(5);

        given(customerDetailRepository.findAllByCondition(pageable,where)).willReturn(resultPage);
        mockMvc.perform(get("/v1/customers").params(where)).andExpect(status().isOk())
                .andExpect(jsonPath("$.code").value("R_200"))
                .andExpect(jsonPath("$.total_items").value(5))
                .andExpect(jsonPath("$.items[0].id").value("A0"));
    }
    // test tạo khách hàng thành công, code 201
    @Test
    public void testCreateCustomerSuccess() throws Exception {
        given(customerRepository.save(isA(Customer.class))).willAnswer(i -> i.getArgument(0));
        given(customerRepository.existsByCode("code")).willReturn(false);
        mockMvc.perform(post("/v1/customers").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_201"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("CREATED"));

    }
    // test tạo khách hàng thất bại với mã khách hàng đã tồn tại, code 400
    @Test
    public void testCreateCustomerFalseWithCustomerIdExists()throws Exception{
        given(customerRepository.existsByCode("C01")).willReturn(true);
        mockMvc.perform(post("/v1/customers").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_400"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Mã khách hàng đã tồn tại"));

    }
    // test cập nhật khách hàng thành công , code 200
    @Test
    public void testUpdateCustomerSuccess() throws Exception {
        Customer customer = new Customer().setId("id").setName("name").setEmail("email")
                .setAddress("address").setTotalAmount(BigDecimal.valueOf(100)).setPhone("09231").setDistric("tx")
                .setCode("code");
        given(customerRepository.findById("id")).willReturn(Optional.of(customer));
        given(customerRepository.save(isA(Customer.class))).willAnswer(i -> i.getArgument(0));
        mockMvc.perform(patch("/v1/customers/{customer_id}","id").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_200"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("OK"));

    }
    // test cập nhật khách hàng thất bại với mã khách hàng không tồn tại, code 404
    @Test
    public void testUpdateCustomerFalseWithCustomerIdNotExists() throws Exception {
        given(customerRepository.findById("10001")).willReturn(Optional.empty());
        mockMvc.perform(patch("/v1/customers/{customer_id}","10001").contentType(APPLICATION_JSON_UTF8).content(jsonCreateRequest))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("R_404"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Mã khách hàng không tồn tại"));

    }

}
