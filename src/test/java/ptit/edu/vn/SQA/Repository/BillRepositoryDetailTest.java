package ptit.edu.vn.SQA.Repository;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Customer;
import ptit.edu.vn.SQA.repository.BillDetailRepository;
import ptit.edu.vn.SQA.repository.BillRepository;
import ptit.edu.vn.SQA.repository.CustomerDetailRepository;
import ptit.edu.vn.SQA.repository.CustomerRepository;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class BillRepositoryDetailTest {
    @TestConfiguration
    public static class BillDetailDetailRepositoryConfiguration {

        @Bean
        BillDetailRepository billDetailRepository() {
            return new BillDetailRepository();
        }

    }
    @Autowired
    BillDetailRepository billDetailRepository;
    @Autowired
    BillRepository billRepository;
    @Autowired
    CustomerRepository customerRepository;

    @Test
    public void testGetAllBill(){
        String[] embed = new String[1];
        embed[0]="customer";
        Pageable pageable= PageRequest.of(0,10);
            Bill bill = new Bill().setId("A").setCustomerCode("code").setAmount(10f).setCreateAt(Instant.now()).setFinishDate(Instant.now())
                    .setStartDate(Instant.now()).setNewNumber(10f).setOldNumber(0f).setMonth(4).setYear(2021);
            bill = billRepository.save(bill);

        Customer customer = new Customer().setId("id").setName("name").setEmail("email")
                .setAddress("address").setTotalAmount(BigDecimal.valueOf(100)).setPhone("09231").setDistric("tx")
                .setCode("code");
        customer =customerRepository.save(customer);
        MultiValueMap<String,String> where = new LinkedMultiValueMap<>();
        ResultPage<Object[]>resultPage = billDetailRepository.getAllWithFilter(pageable,where);
        Object[] returnObject = (Object[]) resultPage.getPageList().get(0);
        Bill bill1 = (Bill) returnObject[0];
        Customer customer1 = (Customer) returnObject[1];

        where.add("customer.code","code");
        assertEquals(1,billDetailRepository.getAllWithFilter(pageable, where).getTotalItems());
    }
//  Sau mỗi test sẽ chạy vào hàm này để clear data test
    @AfterEach
    public void clearDatabase(){
        billRepository.deleteAll();
    }

}
