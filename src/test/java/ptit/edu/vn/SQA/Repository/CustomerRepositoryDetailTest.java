package ptit.edu.vn.SQA.Repository;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.model.Customer;
import ptit.edu.vn.SQA.repository.CustomerDetailRepository;
import ptit.edu.vn.SQA.repository.CustomerRepository;


import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class CustomerRepositoryDetailTest {
    @TestConfiguration
    public static class CustomerDetailDetailRepositoryConfiguration {

        @Bean
        CustomerDetailRepository customerDetailRepository() {
            return new CustomerDetailRepository();
        }

    }
    @Autowired
    CustomerDetailRepository customerDetailRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Test
    public void testGetAllWithCondition(){
        List<Customer> customers = new ArrayList<>();
        for (int i=0;i<10;i++){
            Customer customer= new Customer();
            customer.setId("a"+i);
            customer.setCode("a"+i);
            customer.setAddress("a"+i);
            customer.setDistric("a"+i);
            customer.setEmail("e"+i);
            customer.setName("a"+i);
            customer.setPhone("036302393"+i);
            customer.setTotalAmount(BigDecimal.valueOf(1).add(BigDecimal.valueOf(i)));
            customer.setActiveAt(Instant.now());
            customer.setUnActiveAt(Instant.now());
            customer.setIsActive(true);
            customer = customerRepository.save(customer);
            customers.add(customer);
        }
        Pageable pageable = PageRequest.of(0,10);
        MultiValueMap<String, String> where = new LinkedMultiValueMap<String, String>();
        where.add("name","a");
        assertEquals(10,customerDetailRepository.findAllByCondition(pageable, where).getTotalItems());

        where.clear();
        where.add("id","a0");
        assertEquals(0,customerDetailRepository.findAllByCondition(pageable, where).getTotalItems());
    }
    //  Sau mỗi test sẽ chạy vào hàm này để clear data test
    @AfterEach
    public void clearDatabase(){
        customerRepository.deleteAll();
    }
}
