package ptit.edu.vn.SQA.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ptit.edu.vn.SQA.common.enums.ResponseCodeEnum;
import ptit.edu.vn.SQA.controller.ressponse.ResponseBodyDto;
import ptit.edu.vn.SQA.dto.ConfigPriceDto;
import ptit.edu.vn.SQA.service.ConfigPriceService;

@RestController
@RequestMapping("/v1")
@Validated
public class ConfigPriceController {
    @Autowired
    ConfigPriceService configPriceService;

    @GetMapping(value = "/configs/{config_level}/config-prices", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<ConfigPriceDto>> getProductPrices(
            @PathVariable(name = "config_level") String configId,
            @RequestParam(name = "update_at_start", required = false) Long updateAtStart,
            @RequestParam(name = "update_at_end", required = false) Long updateAtEnd,
            @RequestParam(name = "is_active", required = false) Boolean isActive,
            Pageable pageable) {

        Page<ConfigPriceDto> page = configPriceService.getAllByCondition(configId, updateAtStart, updateAtEnd,
                isActive, pageable);

        ResponseBodyDto<ConfigPriceDto> res = new ResponseBodyDto<ConfigPriceDto>(pageable, page,
                ResponseCodeEnum.R_200, "OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
}
