package ptit.edu.vn.SQA.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ptit.edu.vn.SQA.common.enums.ResponseCodeEnum;
import ptit.edu.vn.SQA.controller.ressponse.ResponseBodyDto;
import ptit.edu.vn.SQA.dto.BillDto;
import ptit.edu.vn.SQA.model.Mail;
import ptit.edu.vn.SQA.service.MailService;

import java.util.List;

@RestController
@RequestMapping("/v1")
@Validated
public class MailController {
    @Autowired
    MailService mailService;
    @PostMapping(value = "/send-mails",produces = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<ResponseBodyDto<List<Mail>>> sendMail(@RequestBody List<BillDto> billDtos){
        List<Mail> mail=   mailService.sendMail(billDtos);
    ResponseBodyDto<List<Mail>> res= new ResponseBodyDto<List<Mail>>(mail, ResponseCodeEnum.R_201,"SEND");
    return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }
    @PostMapping(value = "/report-mails",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<List<Mail>>> reportMail(@RequestBody List<BillDto> billDtos){
        List<Mail> mail=   mailService.report(billDtos);
        ResponseBodyDto<List<Mail>> res= new ResponseBodyDto<List<Mail>>(mail, ResponseCodeEnum.R_201,"SEND");
        return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }

}
