package ptit.edu.vn.SQA.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ptit.edu.vn.SQA.common.enums.ResponseCodeEnum;
import ptit.edu.vn.SQA.controller.request.ConfigCreateRequest;
import ptit.edu.vn.SQA.controller.request.ConfigUpdateRequest;
import ptit.edu.vn.SQA.controller.request.CustomerCreateRequest;
import ptit.edu.vn.SQA.controller.request.CustomerUpdateRequest;
import ptit.edu.vn.SQA.controller.ressponse.ResponseBodyDto;
import ptit.edu.vn.SQA.dto.ConfigDto;
import ptit.edu.vn.SQA.dto.CustomerDto;
import ptit.edu.vn.SQA.service.ConfigService;

import javax.annotation.Resource;

@RestController
@RequestMapping("/v1")
@Validated
public class ConfigController {
    @Autowired
    ConfigService configService;


    // create new Customer
    @PostMapping(value = "/configs",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<ConfigDto>> createConfig(
            @RequestBody ConfigCreateRequest request
    ){
        ConfigDto configDto= configService.create(request);
        ResponseBodyDto<ConfigDto> res= new ResponseBodyDto<ConfigDto>(configDto, ResponseCodeEnum.R_201,"CREATED");
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }
    //update cusstomer
    @PatchMapping(value = "/configs/{config_level}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<ConfigDto>> updateConfig(
            @PathVariable(name = "config_level") String configLevel,
            @RequestBody ConfigUpdateRequest request
    ){
        ConfigDto configDto= configService.update(configLevel,request);
        ResponseBodyDto<ConfigDto> res= new ResponseBodyDto<ConfigDto>(configDto,ResponseCodeEnum.R_200,"OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @GetMapping(value = "/configs", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<ConfigDto>> getAllCustomer(
            @RequestParam(name= "search",required = false) String search,
            @RequestParam(name = "level",required = false) String level,
            Pageable pageable) {

        Page<ConfigDto> page = configService.getAllByCondition(search, level, pageable);

        ResponseBodyDto<ConfigDto> res = new ResponseBodyDto<ConfigDto>(pageable, page, ResponseCodeEnum.R_200, "OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
}


