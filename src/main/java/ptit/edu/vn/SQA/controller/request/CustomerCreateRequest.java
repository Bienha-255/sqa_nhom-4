package ptit.edu.vn.SQA.controller.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CustomerCreateRequest {

    @NotNull(message = "code cannot be null")
    private String code;
    @NotNull(message = "identity_number cannot be null")
    private String identityNumber;
    @NotNull(message = "active_at cannot be null")
    private Instant activeAt;
    @NotNull(message = "name cannot be null")
    private String name;
    @NotNull(message = "email cannot be null")
    private String email;
    @NotNull(message = "phone cannot be null")
    private String phone;
    @NotNull(message = "distric cannot be null")
    private String distric;
    @NotNull(message = "total_amount cannot be null")
    private BigDecimal totalAmount;
    @NotNull(message = "address cannot be null")
    private String address;
}
