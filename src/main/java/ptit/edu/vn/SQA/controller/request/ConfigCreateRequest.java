package ptit.edu.vn.SQA.controller.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConfigCreateRequest {

    @NotNull(message = "level cannot be null")
    private String level;

    @NotNull(message = "price cannot be null")
    private BigDecimal price;
    @NotNull(message = "explain cannot be null")
    private String explain;
    @NotNull(message = "number_min cannot be null")
    private BigDecimal numberMin;
    @NotNull(message = "number_max cannot be null")
    private BigDecimal numberMax;

}
