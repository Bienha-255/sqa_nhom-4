package ptit.edu.vn.SQA.controller.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BillCreateRequest {

    @NotNull(message = "amount cannot be null")
    private Float amount;

    @NotNull(message = "start_date cannot be null")
    private Instant startDate;

    @NotNull(message = "finish_date cannot be null")
    private Instant finishDate;

    @NotNull(message = "new_number cannot be null")
    private Float newNumber;

    @NotNull(message = "old_number cannot be null")
    private Float oldNumber;

    @NotNull(message = "total_price cannot be null")
    private BigDecimal totalPrice;

    @NotNull(message = "year cannot be null")
    private Integer year;

    @NotNull(message = "month cannot be null")
    private Integer month;

}
