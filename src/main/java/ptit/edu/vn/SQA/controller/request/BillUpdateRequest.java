package ptit.edu.vn.SQA.controller.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BillUpdateRequest {

    private String customerCode;

    private Float amount;

    private Instant startDate;
    private Instant finishDate;
    private Float newNumber;
    private Float oldNumber;
    private Integer year;
    private Integer month;
    private BigDecimal totalPrice;
    private Instant paymentDate;
    private String status;
}
