package ptit.edu.vn.SQA.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ptit.edu.vn.SQA.common.enums.ResponseCodeEnum;
import ptit.edu.vn.SQA.controller.request.CustomerCreateRequest;
import ptit.edu.vn.SQA.controller.request.CustomerUpdateRequest;
import ptit.edu.vn.SQA.controller.ressponse.ResponseBodyDto;
import ptit.edu.vn.SQA.dto.CustomerDto;
import ptit.edu.vn.SQA.repository.CustomerRepository;
import ptit.edu.vn.SQA.service.CustomerService;

import javax.ws.rs.QueryParam;

@RestController
@RequestMapping("/v1")
@Validated
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<CustomerDto>> getAllCustomer(
            @RequestParam MultiValueMap<String,String> where,
            @QueryParam("embed")String[] embed,
            Pageable pageable) {

        Page<CustomerDto> page = customerService.getAllByCondition(where,embed, pageable);

        ResponseBodyDto<CustomerDto> res = new ResponseBodyDto<CustomerDto>(pageable, page, ResponseCodeEnum.R_200, "OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
    //create new customer
    // create new Customer
    @PostMapping(value = "/customers",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<CustomerDto>> createCustomer(
            @RequestBody CustomerCreateRequest request
    ){
        CustomerDto customerDto= customerService.createCustomer(request);
        ResponseBodyDto<CustomerDto> res= new ResponseBodyDto<CustomerDto>(customerDto,ResponseCodeEnum.R_201,"CREATED");
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }
    //update cusstomer
    @PatchMapping(value = "/customers/{customer_id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<CustomerDto>> updateCustomer(
            @PathVariable(name = "customer_id") String customerId,
            @RequestBody CustomerUpdateRequest request
    ){
        CustomerDto customerDto= customerService.updateCustomer(customerId,request);
        ResponseBodyDto<CustomerDto> res= new ResponseBodyDto<CustomerDto>(customerDto,ResponseCodeEnum.R_200,"OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
}
