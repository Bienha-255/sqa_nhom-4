package ptit.edu.vn.SQA.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ptit.edu.vn.SQA.common.enums.ResponseCodeEnum;
import ptit.edu.vn.SQA.controller.request.BillCreateRequest;
import ptit.edu.vn.SQA.controller.request.BillUpdateRequest;
import ptit.edu.vn.SQA.controller.request.CustomerCreateRequest;
import ptit.edu.vn.SQA.controller.request.CustomerUpdateRequest;
import ptit.edu.vn.SQA.controller.ressponse.ResponseBodyDto;
import ptit.edu.vn.SQA.dto.BillDto;
import ptit.edu.vn.SQA.dto.CustomerDto;
import ptit.edu.vn.SQA.repository.BillRepository;
import ptit.edu.vn.SQA.service.BillService;

@RestController
@RequestMapping("/v1")
@Validated
public class BillController
{
    @Autowired
    BillService billService;
    @Autowired
    BillRepository billRepository;
    // create new bill
    @PostMapping(value = "/customers/{customer_code}/bills",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<BillDto>> createCustomer(
            @RequestBody BillCreateRequest request,
            @PathVariable("customer_code") String customerCode
    ){
        BillDto billDto= billService.create(customerCode,request);
        ResponseBodyDto<BillDto> res= new ResponseBodyDto<BillDto>(billDto, ResponseCodeEnum.R_201,"CREATED");
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }
    //get bill with customer_code
    @GetMapping(value = "/customers/{customer_code}/bills", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<BillDto>> getAllByCondition(
            @PathVariable(name="customer_code") String customerCode,
            @RequestParam(name = "amount", required = false) Float amount,
            @RequestParam(name = "start_date", required = false) Long startDate,
            @RequestParam(name = "finish_date", required = false) Long finishDate,
            @RequestParam(name = "start_number", required = false) Float startNumber,
            @RequestParam(name = "finish_number", required = false) Float finishNumber,

            Pageable pageable) {

        Page<BillDto> page = billService.getAllByCondition(customerCode,  amount, startDate, finishDate, startNumber, finishNumber,  pageable);

        ResponseBodyDto<BillDto> res = new ResponseBodyDto<BillDto>(pageable, page, ResponseCodeEnum.R_200, "OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    //update bill
    @PatchMapping(value = "/bills/{bill_id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<BillDto>> updateBill(
            @PathVariable(value = "bill_id") String billId,
            @RequestBody BillUpdateRequest request
    ){
        BillDto billDto= billService.update(billId,request);
        ResponseBodyDto<BillDto> res= new ResponseBodyDto<BillDto>(billDto,ResponseCodeEnum.R_200,"OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
    // get all bill
    @GetMapping(value = "/bills",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseBodyDto<BillDto>> getAll(
            @RequestParam MultiValueMap<String,String> where,
            Pageable pageable
            ){
        Page<BillDto> billDto = billService.getAll(pageable,where);
        ResponseBodyDto<BillDto> res = new ResponseBodyDto<BillDto>(pageable, billDto, ResponseCodeEnum.R_200, "OK");
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
}
