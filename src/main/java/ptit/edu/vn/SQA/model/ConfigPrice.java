package ptit.edu.vn.SQA.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Table(name = "config_price")
public class ConfigPrice {

    @Id
    @Column(name = "id",unique = true)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    @Column(name = "config_level")
    private String configLevel;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "create_at", columnDefinition = "datetime")
    private Instant createAt;

    @Column(name = "update_at",columnDefinition = "datetime")
    private Instant updateAt;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "note")
    private String note;
}
