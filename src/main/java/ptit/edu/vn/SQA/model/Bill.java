package ptit.edu.vn.SQA.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Table(name = "bill")
public class Bill {

    @Id
    @Column(name = "id",unique = true,nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    @Column(name = "customer_code")
    private String customerCode;

    @Column(name = "amount")
    private Float amount;

    @Column(name = "start_date",columnDefinition = "datetime")
    private Instant startDate;

    @Column(name = "finish_date",columnDefinition = "datetime")
    private Instant finishDate;

    @Column(name = "month")
    private Integer month;

    @Column(name = "year")
    private Integer year;

    @Column(name = "new_number")
    private Float newNumber;

    @Column(name = "old_number")
    private Float oldNumber;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "payment_date")
    private Instant paymentDate;

    @Column(name = "status")
    private String status;

    @Column(name = "create_at")
    private Instant createAt;

}
