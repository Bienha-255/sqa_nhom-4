package ptit.edu.vn.SQA.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Table(name = "customer")
public class Customer {

    @Id
    @Column(name = "id",unique = true)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    @Column(name = "code")
    private String code;
    @Column(name = "identity_number")
    private String identityNumber;


    @Column(name = "email")
    private String email;

    @Column(name = "name",columnDefinition = "nvarchar")
    private String name;

    @Column(name = "distric",columnDefinition = "nvarchar")
    private String distric;

    @Column(name = "address",columnDefinition = "nvarchar")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;

    @Column(name = "active_at")
    private Instant activeAt;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "un_active_at")
    private Instant unActiveAt;
}
