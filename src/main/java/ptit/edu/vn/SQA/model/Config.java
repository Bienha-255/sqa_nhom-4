package ptit.edu.vn.SQA.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Table(name = "config")
public class Config {
    @Id
    @Column(name = "id",unique = true)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    @Column(name = "level")
    private String level;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "explain",columnDefinition = "nvarchar")
    private String explain;

    @Column(name = "number_min")
    private Integer numberMin;

    @Column(name = "number_max")
    private Integer numberMax;


}
