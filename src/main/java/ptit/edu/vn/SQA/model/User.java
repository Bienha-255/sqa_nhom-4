//package ptit.edu.vn.SQA.model;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import lombok.experimental.Accessors;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//
//@Entity
//@Data
//@Accessors(chain = true)
//@Table(name = "user")
//public class User {
//    @Id
//    @Column(name = "id",unique = true)
//    private String id;
//
//    @Column(name = "username")
//    private  String userName;
//
//    @Column(name = "password")
//    private  String passWord;
//
//}
