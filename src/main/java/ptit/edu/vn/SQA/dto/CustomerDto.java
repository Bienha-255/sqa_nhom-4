package ptit.edu.vn.SQA.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.Instant;
import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CustomerDto {
    private String id;

    private String name;

    private String identityNumber;

    private String distric;

    private  String phone;

    private String address;

    private String email;

    private String code;

    private Float totalAmount;

    private Instant activeAt;

    private List<BillDto> bills;
}
