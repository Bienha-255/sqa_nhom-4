package ptit.edu.vn.SQA.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.Instant;


@Data
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BillDto {
    private String id;

    private String customerCode;

    private Float amount;

    private Float newNumber;

    private Float oldNumber;

    private Instant startDate;

    private Instant finishDate;

    private BigDecimal totalPrice;

    private Integer month;

    private Integer year;

    private CustomerDto customer;


    private Instant paymentDate;

    private String status;
}
