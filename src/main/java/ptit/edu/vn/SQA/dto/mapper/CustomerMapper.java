//package ptit.edu.vn.SQA.dto.mapper;
//
//import org.modelmapper.Conditions;
//import org.modelmapper.ModelMapper;
//import org.modelmapper.convention.MatchingStrategies;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import ptit.edu.vn.SQA.dto.BillDto;
//import ptit.edu.vn.SQA.dto.CustomerDto;
//import ptit.edu.vn.SQA.model.Bill;
//import ptit.edu.vn.SQA.model.Customer;
//
//@Component
//public class CustomerMapper {
//    @Autowired
//    ModelMapper modelMapper;
//
//    public CustomerDto fromModelToDto(Customer customer) {
//        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull())
//                .setMatchingStrategy(MatchingStrategies.STRICT);
//        return modelMapper.map(customer, CustomerDto.class);
//    }
//}
