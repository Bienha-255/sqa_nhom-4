//package ptit.edu.vn.SQA.dto.mapper;
//
//import org.modelmapper.Conditions;
//import org.modelmapper.ModelMapper;
//import org.modelmapper.convention.MatchingStrategies;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import ptit.edu.vn.SQA.dto.BillDto;
//import ptit.edu.vn.SQA.model.Bill;
//
//@Component
//public class BillMapper {
//    @Autowired
//    ModelMapper modelMapper;
//
//    public BillDto fromModelToDto(Bill bill) {
//        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull())
//                .setMatchingStrategy(MatchingStrategies.STRICT);
//        return modelMapper.map(bill, BillDto.class);
//    }
//}
