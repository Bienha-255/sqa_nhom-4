package ptit.edu.vn.SQA.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ptit.edu.vn.SQA.dto.ConfigDto;
import ptit.edu.vn.SQA.dto.ConfigPriceDto;

public interface ConfigPriceService {
    Page<ConfigPriceDto> getAllByCondition(String cogfigId, Long activeAtStart, Long activeAtEnd,
                                           Boolean isActive,
                                           Pageable pageable);
}
