package ptit.edu.vn.SQA.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.common.error.BadRequestException;
import ptit.edu.vn.SQA.common.error.NotFoundException;
import ptit.edu.vn.SQA.controller.request.BillCreateRequest;
import ptit.edu.vn.SQA.controller.request.BillUpdateRequest;
import ptit.edu.vn.SQA.dto.BillDto;
import ptit.edu.vn.SQA.dto.CustomerDto;
import ptit.edu.vn.SQA.dto.mapper.CommonMapper;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Config;
import ptit.edu.vn.SQA.model.Customer;
import ptit.edu.vn.SQA.repository.BillDetailRepository;
import ptit.edu.vn.SQA.repository.BillRepository;
import ptit.edu.vn.SQA.repository.ConfigRepository;
import ptit.edu.vn.SQA.repository.CustomerRepository;
import ptit.edu.vn.SQA.service.BillService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class BillServiceImpl implements BillService
{

    @Autowired
    private BillRepository billRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private BillDetailRepository billDetailRepository;
    @Override
    public BillDto create(String customerId, BillCreateRequest request) {
        if(!customerRepository.existsByCode(customerId)){
            throw new NotFoundException("Mã khách hàng không tồn tại");
        }
        Bill bill = new Bill();
        CommonMapper.copyPropertiesIgnoreNull(request,bill);
        bill.setCustomerCode(customerId);
        billRepository.save(bill);
        return CommonMapper.map(bill,BillDto.class);
    }

    @Override
    public BillDto update(String billId, BillUpdateRequest request) {
        Optional<Bill> optional = billRepository.findById(billId);
        if (!optional.isPresent()) {
            throw new NotFoundException("Mã hóa đơn không tồn tại");
        }
        Bill bill = optional.get();
        CommonMapper.copyPropertiesIgnoreNull(request,bill);
        bill = billRepository.save(bill);
        return CommonMapper.map(bill, BillDto.class);
    }

    @Override
    public Page<BillDto> getAll(Pageable pageable, MultiValueMap<String, String> where) {
        ResultPage<Object[]> page= billDetailRepository.getAllWithFilter(pageable,where);
        List<BillDto> billDtos = new ArrayList<>();
        for (Object[] objects : page.getPageList()) {
            Bill bill = (Bill) objects[0];
            BillDto billDto = CommonMapper.map(bill,BillDto.class);
            if(objects[1] != null){
                Customer customer = (Customer) objects[1];
                CustomerDto customerDto = CommonMapper.map(customer,CustomerDto.class);
                billDto.setCustomer(customerDto);
            }
            billDtos.add(billDto);
        }
        return new PageImpl<BillDto>(billDtos, pageable, page.getTotalItems());

    }

    @Override
    public Page<BillDto> getAllByCondition(String customerId, Float amount, Long startDate, Long finishDate, Float startNumber, Float finishNumber, Pageable pageable) {
         Page<Bill> page= billRepository.getAllByCondition(customerId,startDate,finishDate,startNumber,finishNumber,amount,pageable);
        return CommonMapper.toPage(page, BillDto.class, pageable);
    }

}
