package ptit.edu.vn.SQA.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ptit.edu.vn.SQA.common.error.BadRequestException;
import ptit.edu.vn.SQA.common.error.NotFoundException;
import ptit.edu.vn.SQA.controller.request.ConfigCreateRequest;
import ptit.edu.vn.SQA.controller.request.ConfigUpdateRequest;
import ptit.edu.vn.SQA.dto.ConfigDto;
import ptit.edu.vn.SQA.dto.mapper.CommonMapper;
import ptit.edu.vn.SQA.model.Config;
import ptit.edu.vn.SQA.model.ConfigPrice;
import ptit.edu.vn.SQA.repository.ConfigPriceRepository;
import ptit.edu.vn.SQA.repository.ConfigRepository;
import ptit.edu.vn.SQA.service.ConfigService;

import java.time.Instant;
import java.util.Optional;

@Service
public class ConfigServiceImpl implements ConfigService {
    @Autowired
    ConfigRepository configRepository;
    @Autowired
    ConfigPriceRepository configPriceRepository;
    @Override
    public ConfigDto create(ConfigCreateRequest request) {
    if (configRepository.existsByLevel(request.getLevel())){
        throw new BadRequestException("Bậc nước đã tồn tại");
    }

        Config config = CommonMapper.map(request,Config.class);
        config = configRepository.save(config);

        ConfigPrice configPrice = CommonMapper.map(request,ConfigPrice.class);
        configPrice.setConfigLevel(config.getLevel());
        configPrice.setPrice(request.getPrice());
        Instant now = Instant.now();
        configPrice.setIsActive(true);
        configPrice.setCreateAt(now);
        configPrice.setUpdateAt(now);
        configPriceRepository.save(configPrice);
        return CommonMapper.map(config,ConfigDto.class);
    }
// cập nhật cấu hình theo level
    @Override
    public ConfigDto update(String level, ConfigUpdateRequest request) {
        //update lại cấu hình theo level
        Optional<Config> optional= configRepository.findByLevel(level);
        if (!optional.isPresent()){
            throw new NotFoundException("Ma khong ton tai");
        }
        Config config= optional.get();
        CommonMapper.copyPropertiesIgnoreNull(request,config);
       config= configRepository.save(config);

        //gen ra lịch sử thay đổi giá bậc nước
        ConfigPrice configPriceOld = configPriceRepository.findLatestConfigPrice(level);
        if (configPriceOld!=null){
            configPriceOld.setIsActive(false);
            configPriceRepository.save(configPriceOld);
        }
        ConfigPrice newConfigPrice = new ConfigPrice();
        CommonMapper.copyPropertiesIgnoreNull(request,newConfigPrice);
        newConfigPrice.setConfigLevel(level);
        newConfigPrice.setIsActive(true);
        Instant now= Instant.now();
        newConfigPrice.setPrice(request.getPrice());
        newConfigPrice.setUpdateAt(now);
        newConfigPrice.setCreateAt(configPriceOld.getCreateAt());
        configPriceRepository.save(newConfigPrice);
        return CommonMapper.map(config,ConfigDto.class);

    }

    @Override
    public Page<ConfigDto> getAllByCondition(String search, String level, Pageable pageable) {
        Page<Config> page= configRepository.getAllByCondition(search,level,pageable);
        return CommonMapper.toPage(page, ConfigDto.class, pageable);
    }
}
