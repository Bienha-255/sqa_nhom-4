package ptit.edu.vn.SQA.service;


import ptit.edu.vn.SQA.dto.BillDto;
import ptit.edu.vn.SQA.model.Mail;

import java.util.List;

public interface MailService {
    List<Mail> sendMail(List<BillDto> billDtos);

    List<Mail> report(List<BillDto> billDtos);
}
