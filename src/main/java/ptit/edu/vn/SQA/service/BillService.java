package ptit.edu.vn.SQA.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.controller.request.BillCreateRequest;
import ptit.edu.vn.SQA.controller.request.BillUpdateRequest;
import ptit.edu.vn.SQA.dto.BillDto;
import ptit.edu.vn.SQA.model.Bill;

public interface BillService
{
    BillDto create(String customerId,BillCreateRequest request);
    BillDto update(String billId, BillUpdateRequest request);
    Page<BillDto> getAll(Pageable pageable, MultiValueMap<String,String> where);
    Page<BillDto> getAllByCondition(String customerId, Float amount, Long startDate, Long finishDate, Float startNumber, Float finishNumber, Pageable pageable);
}
