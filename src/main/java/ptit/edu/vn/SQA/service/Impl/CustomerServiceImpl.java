package ptit.edu.vn.SQA.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.common.error.BadRequestException;
import ptit.edu.vn.SQA.common.error.NotFoundException;
import ptit.edu.vn.SQA.controller.request.CustomerCreateRequest;
import ptit.edu.vn.SQA.controller.request.CustomerUpdateRequest;
import ptit.edu.vn.SQA.dto.BillDto;
import ptit.edu.vn.SQA.dto.CustomerDto;

import ptit.edu.vn.SQA.dto.mapper.CommonMapper;

import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Customer;
import ptit.edu.vn.SQA.repository.BillRepository;
import ptit.edu.vn.SQA.repository.CustomerDetailRepository;
import ptit.edu.vn.SQA.repository.CustomerRepository;
import ptit.edu.vn.SQA.service.CustomerService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    CustomerDetailRepository customerDetailRepository;
    @Autowired
    BillRepository billRepository;


    @Override
    public Page<CustomerDto> getAllByCondition(MultiValueMap<String, String> where, String[] embed,Pageable pageable) {
        ResultPage<Customer> rs = customerDetailRepository.findAllByCondition(pageable, where);
        long total = rs.getTotalItems();
        List<Customer> customers = rs.getPageList();

        if (embed == null) {
            return new PageImpl<CustomerDto>(
                    customers.stream().map(customer -> CommonMapper.map(customer, CustomerDto.class)).collect(Collectors.toList()),
                    pageable, total);
        }

        // Check embed list
        List<String> embedTable = new ArrayList<>();
        List<String> allTable = new ArrayList<String>(Arrays.asList("bill"));
        for (String i : embed) {
            if (!allTable.contains(i))
                throw new BadRequestException("Table name \'" + i + "\' cannot be obtained!");
            if (!embedTable.contains(i))
                embedTable.add(i);
        }

        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer customer : customers) {

            CustomerDto customerDto = CommonMapper.map(customer, CustomerDto.class);
            if (embedTable.contains("bill")) {
                List<Bill> bills = billRepository.getAllByCustomerCode(customerDto.getCode());
                if(bills != null && bills.size() != 0){
                    customerDto.setBills(bills.stream().map(bill -> CommonMapper.map(bill, BillDto.class)).collect(Collectors.toList()));
                }
            }
            customerDtos.add(customerDto);
        }
        return new PageImpl<CustomerDto>(customerDtos,pageable,total);
    }

    @Override
        public CustomerDto createCustomer(CustomerCreateRequest request) {
            if(customerRepository.existsByCode(request.getCode())){
                throw new BadRequestException("Mã khách hàng đã tồn tại");
            }

            Customer customer = new Customer();
            CommonMapper.copyPropertiesIgnoreNull(request,customer);
            customer=customerRepository.save(customer);
            return CommonMapper.map(customer,CustomerDto.class);

        }

    //update customer
    @Override
    public CustomerDto updateCustomer(String customerId, CustomerUpdateRequest request) {
        Optional<Customer> optional = customerRepository.findById(customerId);
        if (!optional.isPresent()) {
            throw new NotFoundException("Mã khách hàng không tồn tại");
        }
        Customer customer = optional.get();
        CommonMapper.copyPropertiesIgnoreNull(request,customer);
        customer = customerRepository.save(customer);
        return CommonMapper.map(customer, CustomerDto.class);
    }
}

