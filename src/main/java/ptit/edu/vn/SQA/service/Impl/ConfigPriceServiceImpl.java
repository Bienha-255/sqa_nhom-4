package ptit.edu.vn.SQA.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ptit.edu.vn.SQA.common.error.BadRequestException;
import ptit.edu.vn.SQA.dto.ConfigDto;
import ptit.edu.vn.SQA.dto.ConfigPriceDto;
import ptit.edu.vn.SQA.dto.mapper.CommonMapper;
import ptit.edu.vn.SQA.model.ConfigPrice;
import ptit.edu.vn.SQA.repository.ConfigPriceRepository;
import ptit.edu.vn.SQA.repository.ConfigRepository;
import ptit.edu.vn.SQA.service.ConfigPriceService;

import java.time.Instant;

@Service
public class ConfigPriceServiceImpl implements ConfigPriceService {
    @Autowired
    ConfigRepository configRepository;
    @Autowired
    ConfigPriceRepository configPriceRepository;
    @Override
    public Page<ConfigPriceDto> getAllByCondition(String cogfigLevel, Long activeAtStart, Long activeAtEnd, Boolean isActive, Pageable pageable) {
//        Instant updateAtStart = activeAtStart == null ? null : Instant.ofEpochSecond(activeAtStart);
//        Instant updateAtEnd = activeAtEnd == null ? null : Instant.ofEpochSecond(activeAtEnd);
        if (!configRepository.existsByLevel(cogfigLevel)){
            throw new BadRequestException("Bậc nước không tồn tại");
        }
        Page<ConfigPrice> page = configPriceRepository.getAllByCondition(cogfigLevel, activeAtStart, activeAtEnd, isActive,
                pageable);
        return CommonMapper.toPage(page, ConfigPriceDto.class, pageable);
    }
}
