package ptit.edu.vn.SQA.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ptit.edu.vn.SQA.controller.request.ConfigCreateRequest;
import ptit.edu.vn.SQA.controller.request.ConfigUpdateRequest;
import ptit.edu.vn.SQA.dto.ConfigDto;

public interface ConfigService {
    ConfigDto create(ConfigCreateRequest request);

    ConfigDto update(String level,ConfigUpdateRequest request);

    Page<ConfigDto> getAllByCondition(String search, String level, Pageable pageable);
}
