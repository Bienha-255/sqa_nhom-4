package ptit.edu.vn.SQA.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.controller.request.CustomerCreateRequest;
import ptit.edu.vn.SQA.controller.request.CustomerUpdateRequest;
import ptit.edu.vn.SQA.dto.CustomerDto;
import ptit.edu.vn.SQA.model.Customer;

public interface CustomerService {
    Page<CustomerDto> getAllByCondition(MultiValueMap<String,String> where,String[] embed, Pageable pageable);

    CustomerDto createCustomer(CustomerCreateRequest request);

    CustomerDto updateCustomer(String customerId,CustomerUpdateRequest request);
}
