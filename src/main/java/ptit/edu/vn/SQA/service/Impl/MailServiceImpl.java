package ptit.edu.vn.SQA.service.Impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import ptit.edu.vn.SQA.dto.BillDto;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Mail;
import ptit.edu.vn.SQA.service.MailService;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final JavaMailSender emailSender;
    private final SpringTemplateEngine templateEngine;

    @Value("${spring.mail.username}")
    private String EMAIL_FROM;
    private final String WATER_COMPANY_SIGN = "Công ty nước sạch Hà Nội";
    @Override
    public List<Mail> sendMail(List<BillDto> list) {
        //for loop list đó
        //gửi mail với mỗi bill
        List<Mail> mailList = new ArrayList<>();
        for (BillDto billDto :list) {
            Mail mail = buildMailEntity(billDto);
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = null;
            try {
                helper = new MimeMessageHelper(message,
                        MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                        StandardCharsets.UTF_8.name());
                String html = getHtmlContent(mail);

                helper.setTo(mail.getTo());
                helper.setFrom(EMAIL_FROM);
                helper.setSubject(mail.getSubject());
                helper.setText(html, true);

                emailSender.send(message);
                mailList.add(mail);

            } catch (MessagingException e) {
                throw new RuntimeException("Send email fail", e);
            }
        }
       return mailList;
    }

    @Override
    public List<Mail> report(List<BillDto> billDtos) {
        List<Mail> mailList = new ArrayList<>();
        for (BillDto billDto :billDtos) {
            Mail mail = buildMailReport(billDto);
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = null;
            try {
                helper = new MimeMessageHelper(message,
                        MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                        StandardCharsets.UTF_8.name());
                String html = getHtmlContent(mail);

                helper.setTo(mail.getTo());
                helper.setFrom(EMAIL_FROM);
                helper.setSubject(mail.getSubject());
                helper.setText(html, true);

                emailSender.send(message);
                mailList.add(mail);

            } catch (MessagingException e) {
                throw new RuntimeException("Send email fail", e);
            }
        }
        return mailList;
    }

    private Mail buildMailEntity(BillDto billDto){

        Mail mail = Mail.builder()
                .to(billDto.getCustomer().getEmail())
                .htmlTemplate(new Mail.HtmlTemplate("SendMail", buildMailProperties(billDto)))
                .subject("Thông báo thu tiền nước")
                .build();
        return mail;
    }
    private Mail buildMailReport(BillDto billDto){

        Mail mail = Mail.builder()
                .to(billDto.getCustomer().getEmail())
                .htmlTemplate(new Mail.HtmlTemplate("Report", buildMailProperties(billDto)))
                .subject("Thông báo ngừng cung cấp nước")
                .build();
        return mail;
    }

    private Map<String, Object> buildMailProperties(BillDto billDto){
        Map<String, Object> mailProperties = new HashMap<String, Object>();
        mailProperties.put("name", billDto.getCustomer().getName());
        mailProperties.put("location", billDto.getCustomer().getAddress());
        mailProperties.put("sign", WATER_COMPANY_SIGN);
        mailProperties.put("oldIndex", billDto.getOldNumber());
        mailProperties.put("newIndex", billDto.getNewNumber());
        mailProperties.put("total_price", billDto.getTotalPrice());
        mailProperties.put("tax", (billDto.getTotalPrice().multiply(BigDecimal.valueOf(0.05))));
        mailProperties.put("phone",billDto.getCustomer().getPhone());
        mailProperties.put("amount", billDto.getAmount());
        mailProperties.put("date", billDto.getPaymentDate());
        mailProperties.put("month",billDto.getMonth());
        mailProperties.put("year",billDto.getYear());
        mailProperties.put("totalWithTax", (billDto.getTotalPrice().subtract((billDto.getTotalPrice().multiply(BigDecimal.valueOf(0.05))))));
        return mailProperties;
    }


    private String getHtmlContent(Mail mail) {
        Context context = new Context();
        context.setVariables(mail.getHtmlTemplate().getProps());
        return templateEngine.process(mail.getHtmlTemplate().getTemplate(), context);
    }


//    private float getTotal(int oldIndex, int newIndex){
//        float totalWithoutVAT = (float) ((newIndex - oldIndex) * unitPrice);
//        return totalWithoutVAT;
//    }
//
//    private float getTotalWithTax(int oldIndex, int newIndex){
//        float totalWithoutVAT = (float) ((newIndex - oldIndex) * unitPrice);
//        return (float) (totalWithoutVAT * 1.05);
//    }

}
