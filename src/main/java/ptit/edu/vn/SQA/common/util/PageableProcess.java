package ptit.edu.vn.SQA.common.util;


import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Set;

public class PageableProcess {
    public static String PageToSqlQuery(Pageable pageable, String defaultTable) {
        String page_ = "";
        Sort sort = pageable.getSort();
        Set<Sort.Order> orders = sort.toSet();
        for (Sort.Order order : orders) {
            String prop = order.getProperty();
            if (prop.split("\\.").length == 1) {
                prop = defaultTable + "." + prop;
            }
            if (page_ != "") {
                page_ += " ," + prop;
            } else {
                page_ = " order by " + prop;
            }
            Sort.Direction direction = order.getDirection();
            if (direction.isAscending()) {
                page_ += " asc";
            } else {
                page_ += " desc";
            }
        }
        long offSet = pageable.getOffset();
        long pageSize = pageable.getPageSize();
        if (page_ == "") {
            page_ = " order by (select null)";
        }
        page_ += " OFFSET " + offSet + " ROWS FETCH NEXT " + pageSize + " ROWS ONLY";
        return page_;
    }

    public static String PageToSqlQueryWithOrderBy(Pageable pageable, String defaultTable, String orderby) {
        String page_ = "";
        Sort sort = pageable.getSort();
        Set<Sort.Order> orders = sort.toSet();
        for (Sort.Order order : orders) {
            String prop = order.getProperty();
            if (prop.split("\\.").length == 1) {
                prop = defaultTable + "." + prop;
            }
            if (page_ != "") {
                page_ += " ," + prop;
            } else {
                page_ = " order by " + prop;
            }
            Sort.Direction direction = order.getDirection();
            if (direction.isAscending()) {
                page_ += " asc";
            } else {
                page_ += " desc";
            }
        }
        long offSet = pageable.getOffset();
        long pageSize = pageable.getPageSize();
        if (page_ == "") {
            if(orderby.isEmpty()){
                page_ = " order by (select null)";
            }else{
                page_ = " order by " + orderby;
            }
        }
        page_ += " OFFSET " + offSet + " ROWS FETCH NEXT " + pageSize + " ROWS ONLY";
        return page_;
    }
}

