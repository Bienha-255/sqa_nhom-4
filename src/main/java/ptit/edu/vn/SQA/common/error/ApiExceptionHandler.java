package ptit.edu.vn.SQA.common.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import ptit.edu.vn.SQA.common.enums.ResponseCodeEnum;
import ptit.edu.vn.SQA.controller.ressponse.ResponseBodyDto;

@RestControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(BadRequestException.class)
    public @ResponseBody
    ResponseEntity<ResponseBodyDto<Object>> handleBadRequestException(Exception ex,
                                                                      WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ResponseBodyDto<Object> dtoResult = new ResponseBodyDto<Object>();
        dtoResult.setCode(ResponseCodeEnum.R_400);
        dtoResult.setMessage(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(headers).body(dtoResult);
    }
    @ExceptionHandler(NotFoundException.class)
    public @ResponseBody ResponseEntity<ResponseBodyDto<Object>> handleNotFoundException(Exception ex) {
        // quá trình kiểm soat lỗi diễn ra ở đây
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ResponseBodyDto<Object> dtoResult = new ResponseBodyDto<>();
        dtoResult.setCode(ResponseCodeEnum.R_404);
        dtoResult.setMessage(ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).headers(headers).body(dtoResult);
    }
}
