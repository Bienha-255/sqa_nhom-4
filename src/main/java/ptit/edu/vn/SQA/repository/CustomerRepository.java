package ptit.edu.vn.SQA.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ptit.edu.vn.SQA.model.Customer;
@Repository
public interface CustomerRepository extends JpaRepository<Customer,String> {
    @Query(value =
            "select * from customer co where (:search is null or (co.distric like %:search% or co.name like "
                    + "%:search% or co.id like %:search%))"+ "and (:id is null or co.id like %:id%) "+ "and (:name is null or co.name like %:name%) "
                    + "and (:address is null or co.address like %:address%)"+"and (:phone is null or co.phone like %:phone%)"+"and (:code is null or co.code like %:code%)"
            +"and (:total_amount is null or co.total_amount like %:total_amount%)"+"and (:email is null or co.email like %:email%)"+"and (:distric is null or co.distric like %:distric%)",nativeQuery = true)
    Page<Customer> getAllByCondition(@Param("search") String search, @Param("id") String id, @Param("name") String name, @Param("address") String address, @Param(
            "phone") String phone,@Param("code")String code,@Param("total_amount") String totalAmount,@Param("email")String email,String distric, Pageable pageable);

    Boolean existsByCode(String code);
    Customer findByCode(String code);
}
