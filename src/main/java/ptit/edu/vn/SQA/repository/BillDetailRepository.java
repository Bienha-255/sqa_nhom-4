package ptit.edu.vn.SQA.repository;

import org.hibernate.Session;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.common.util.SqlProcess;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Customer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BillDetailRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public ResultPage<Object[]> getAllWithFilter(Pageable pageable, MultiValueMap<String, String> where) {
        Session session = (Session) entityManager.getDelegate();
        String selectQuery = "select {bill.*},{customer.*} ";
        String fromQuery = "from bill left join customer on bill.customer_code = customer.code ";

        Map<String, Object> queryTypes = new HashMap<>();
        List<String> equalTypes = new ArrayList<>(Arrays.asList("customer.code",
                "customer.distric", "customer.address","customer.email","year","month"));
        queryTypes.put("equalType", equalTypes);
        List<String> likeTypes = new ArrayList<>(Arrays.asList("customer.name",
                "customer.total_amount", "customer.name"));
        queryTypes.put("likeType", likeTypes);
        List<String> dateType = new ArrayList<>(Arrays.asList("customer.active_at"));
        queryTypes.put("dateType", dateType);
//        List<String> inRangeTypes = new ArrayList<String>();
//        queryTypes.put("inRangeType", inRangeTypes);
        // field được cho phép tìm kiếm
        // search
        Map<String, List<String>> searchType = new HashMap<>();
        List<String> stringSearchFields = new ArrayList<>(
                Arrays.asList("year","month","customer_code","amount","total_price"));
        searchType.put("string_search", stringSearchFields);
        queryTypes.put("searchType", searchType);
        List<String> embedTables = new ArrayList<String>(Arrays.asList("customer"));

        HashMap<String, Class<?>> allowEntities = new HashMap<String, Class<?>>();
        allowEntities.put("bill", Bill.class);
        allowEntities.put("customer", Customer.class);
        return SqlProcess.getResultPageWithEmbed(selectQuery, fromQuery, session,
                "bill", allowEntities, pageable, embedTables, where, queryTypes);

    }
}
