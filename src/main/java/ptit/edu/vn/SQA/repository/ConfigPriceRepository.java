package ptit.edu.vn.SQA.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ptit.edu.vn.SQA.model.ConfigPrice;

import java.time.Instant;

public interface ConfigPriceRepository extends JpaRepository<ConfigPrice,String> {
    @Query(value = "select * from config_price cp where  (:config_level is null or cp.config_level=:config_level) " +
            "and (:update_at_start is null or cp.update_at >= :update_at_start) " +
            " and  (:update_at_end is null or cp.update_at <= :update_at_end)"
           +  "and (:is_active is null or cp.is_active =:is_active) ", nativeQuery = true)
    Page<ConfigPrice> getAllByCondition(@Param("config_level") String configLevel, @Param("update_at_start") Long activeAtStart, @Param(
            "update_at_end") Long activeAtEnd, @Param("is_active") Boolean isActive
            , Pageable pageable);

    @Query(value = "select * from config_price cp where  (cp.config_level=:config_level) "
            + "and  (cp.is_active = 1) ", nativeQuery = true)
    ConfigPrice findLatestConfigPrice(@Param("config_level") String configLevel);


}
