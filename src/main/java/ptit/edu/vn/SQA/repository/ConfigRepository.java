package ptit.edu.vn.SQA.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ptit.edu.vn.SQA.model.Config;

import java.util.Optional;

@Repository
public interface ConfigRepository extends JpaRepository<Config,String> {
    Optional<Config> findByLevel(String level);
    Boolean existsByLevel(String level);
    @Query(value = "select * from config cf where (:search is null or cf.level like %:search% or cf.id like %:search% ) and (:level is null or cf.level like %:level%) ", nativeQuery = true)
    Page<Config> getAllByCondition(@Param("search") String search, @Param("level") String level, Pageable pageable);
}
