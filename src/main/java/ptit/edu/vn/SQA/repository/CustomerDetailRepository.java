package ptit.edu.vn.SQA.repository;

import org.hibernate.Session;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.MultiValueMap;
import ptit.edu.vn.SQA.common.constant.ResultPage;
import ptit.edu.vn.SQA.common.util.SqlProcess;
import ptit.edu.vn.SQA.model.Bill;
import ptit.edu.vn.SQA.model.Customer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CustomerDetailRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public ResultPage<Customer> findAllByCondition(Pageable pageable, MultiValueMap<String,String> where){
        Session session = (Session) entityManager.getDelegate();
        List<String> likeType =new ArrayList<>(Arrays.asList("code","email","address","distric","name","phone","total_amount"));
        List<String> equalType = new ArrayList<>(Arrays.asList("id"));
        List<String> dateType = new ArrayList<>(Arrays.asList("active_at","un_active_at"));
        Map<String,Object>queryTypes =new HashMap<>();
        queryTypes.put("likeType",likeType);
        queryTypes.put("equalType",equalType);
        queryTypes.put("dateType",dateType);
        return SqlProcess.getResultPage(session, "customer", Customer.class,
                pageable,  where, queryTypes) ;
    }
}
