package ptit.edu.vn.SQA.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ptit.edu.vn.SQA.model.Bill;

import java.util.List;
@Repository
public interface BillRepository extends JpaRepository<Bill,String> {
    @Query(value = "select * from bill b where  (:customer_code is null or b.customer_code=:customer_code) "
            + "and (:start_date is null or b.start_date like %:start_date%)"+"and (:finish_date is null or b.finish_date like %:finish_date%)"+"and (:new_number is null or b.new_number like %:new_number%)"
            +"and (:old_number is null or b.old_number like %:old_number%)"+"and (:amount is null or b.amount like %:amount%)", nativeQuery = true)
    Page<Bill> getAllByCondition(@Param("customer_code") String customerCode,  @Param("start_date") Long startDate, @Param(
            "finish_date") Long finishDate, @Param("new_number")Float newNumber, @Param("old_number") Float oldNumber, @Param("amount")Float amount, Pageable pageable);

    @Query(value = "select * from bill b where  b.customer_code=:customer_code "
            + "and  b.amount ", nativeQuery = true)
    Bill findLatestAmount(@Param("customer_code") String customerCode);

    List<Bill>getAllByCustomerCode(String customerCode);

}
